import me.d3249.experiment.metaprog.annotations.SayHi;

@SayHi
public class DemoClass {

    private String name;

    public DemoClass(String theName){
        name = theName;
    }

    public String getName(){
        return name;
    }

}
