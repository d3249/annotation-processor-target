# Demo Project

This is a companion project. The main project is a POC for code generation with java annotations

for compiling use the command

```bash
javac -cp helloannotations-1.0-SNAPSHOT.jar:javapoet-1.12.1.jar -processor me.d3249.experiment.metaprog.annotations.SayHiProcessor *.java
```
That should create and compile DemoClassExtended

Java-poet can be downloaded from [maven](https://repo1.maven.org/maven2/com/squareup/javapoet/1.12.1/javapoet-1.12.1.jar)

helloannotations must be generated from the [main project](https://gitlab.com/d3249/annotation-processor)
